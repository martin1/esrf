%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DiNi(2022) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% Function that Read DiniCOM %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% M.Spitoni - Created on 09/02/2023 %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input: SerialPortDini
%output: myH as HeightReading, myD as DistReading, readdini from realine function

function [myH,myD,readdini] = f_readdini(SerialPortDini) %Read 
    disp("f_readdini function ongoing ...")
    writeline(SerialPortDini,"FML");
    readdini = readline(SerialPortDini);

    if ~isempty(readdini)

        disp(readdini)
        if readdini == "E202" || readdini == "E320" || readdini == "E321"...
              || readdini == "E323" || readdini == "E327"%Error
            myH=-999999;
            myD=-999999;
            disp(myH)
            disp(myD)
            disp("f_readdini done but error with Dini")
        else 
            mydata = split(readdini, whitespacePattern).';
            mydata = double(mydata(:,[6,9]));
            myH = mydata(1);
            myD = mydata(2);
            disp(mydata)
            disp(myH)
            disp(myD)
            disp("f_readdini done successfully!")
        end

    elseif isempty(readdini)
        disp("DiNi might be not connected or switched on!")
        myH=-999999;
        myD=-999999;
    end
end