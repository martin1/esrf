%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DiNi(2022) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Function that set params of importfile with custom constraints %%%
%%%%%%%%%%%%%%%%%% M.Spitoni - Created on 09/02/2023 %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input: file is file is a filepath in string (1,1)
%output: stationU contains all unique station from file
%output: 

function [stationU,DataFile] = f_importdinifile(filepath,type)

arguments
    filepath (1,1) string {mustBeFile} % Set constrains of format, vartype and filetype
    type (1,1) string  % Set constrains of format, vartype
end

    % Set up the Import Options and import the data
    opts = delimitedTextImportOptions("NumVariables", 14);%Set up import    
    opts.DataLines = [1, Inf]; % range
    opts.Delimiter = "\t"; % delimiter
    opts.VariableNames = ["Station", "Point", ...
        "Read_1", "Read_2", "Read_3", ...
        "E_12", "E_13", "E_23", ...
        "Distance1", "Distance2", "Distance3", ...
        "Avg_Read", "Avg_Distance", "Device"];
    opts.VariableTypes = ["categorical", "categorical", ...
        "double", "double", "double", ...
        "double", "double", "double", ...
        "double", "double", "double", ...
        "double", "double", "categorical"];

    if type == "Existing file"  %Import Datafile
        DataFile = readtable(filepath, opts);
        station = DataFile(:,1);
        stationU = unique(station,"stable");
        stationU = table2array(stationU);
    else
        DataFile = readtable(filepath, opts);
        DataFile = DataFile(2:end,:);
        station = DataFile(2:end,1);
        stationU = unique(station,"stable");
        stationU = table2array(stationU);
    end

end